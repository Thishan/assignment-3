#include <stdio.h>
int main()
{
  int x, y = 0 ,r;

  printf("Enter a number to reverse\n");
  scanf("%d", &x);

  while (x != 0)
  {
    r = x % 10;
    y = y * 10 + r;
    x /= 10;
  }

  printf("Reverse of the number = %d\n", y);

  return 0;
}
